<?php

echo '<a class="btn btn-primary" href="../../">Back</a>';

require_once 'blocks/bd.php';


$sql = "
SELECT auth.name, books.title 
FROM auth_book 
JOIN auth ON auth.id = auth_book.auth_id 
JOIN books ON books.id = auth_book.book_id
WHERE auth_book.book_id IN (
    SELECT books.id
    FROM books 
    INNER JOIN auth_book ON books.id = auth_book.book_id 
    INNER JOIN auth ON auth_book.auth_id = auth.id
    GROUP BY auth_book.book_id
    HAVING COUNT(auth_book.auth_id) > 1
)
ORDER BY auth_book.book_id
";


$result2 = $pdo->query($sql);


echo "<div class='container'>
        <div class='row'>
            <div class='col-sm-8 col-sm-offset-2'>
            <div class='page-header'>
                    <div class='alert alert-info' role='alert'>
                        <span>Books With (Author)>1</span>
                    </div>
                </div>
<table border='2px'>
<thead>
    <tr>
      <th>Author</th>
      <th>Book</th>
    </tr>
  </thead>
  <tbody>
";


while($myrow2 = $result2->fetch()){
   printf("
    <tr>
    <td>%s</td>
   	<td>%s</td>


    </tr>

    ", $myrow2['name'], $myrow2['title']); 
}

echo "</tbody></table>
            </div>
        </div>
    </div>
";

 
?>

