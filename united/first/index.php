<?php

echo '<a class="btn btn-primary" href="../../">Back</a>';

	/*
	* Создать класс BaseClass который нельзя наследовать
	* @status - свойства для хранения статуса наличия таблицы test в БД 
	*/


	final class BaseClass {
		private $status;


		function __construct()
		{
			
			$this->Create();
			$this->Fill();
		}


		/*
		* Метод Create создаёт таблицу в БД. Вызывается конструктором
		*/
	
		private function Create()
		{
			include("assets/blocks/bd.php");
			
			 			try 
			 			{
					    	
					     	$answer = $pdo->query("SELECT 1 FROM test LIMIT 1"); 
					    } 
					    catch (Exception $e) 
					    {
					         
					        $this->status = TRUE;
					       
					    }
			

						if($this->status){

							$result = $pdo->query('
								SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
								SET AUTOCOMMIT = 0;
								START TRANSACTION;
								SET time_zone = "+00:00";


								CREATE TABLE `test` (
								  `id` int(11) NOT NULL,
								  `script_name` varchar(25) NOT NULL,
								  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
								  `sort_index` int(3) NOT NULL,
								  `result` enum("normal","illegal","failed","success") NOT NULL
								) ENGINE=InnoDB DEFAULT CHARSET=utf8;


								ALTER TABLE `test`
								  ADD PRIMARY KEY (`id`);


								ALTER TABLE `test`
								  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
								COMMIT;

							');
						}
   
		}

		/*
		* Метод Fill создаёт таблицу в БД. Вызывается конструктором
		*/

		private function Fill()
		{
			include("assets/blocks/bd.php");

			if($this->status){

				for($i=0; $i<5; $i++)
				
				{

					$a = rand(0,5);
					$b = rand(1,99);
					$c = rand(0,3);
					$indetific = ['normal', 'illegal', 'failed', 'success'];
					$script_name = ['java', 'python', 'php', 'javascript', 'ruby', 'c++'];


				$intoTable = $pdo->query("
					INSERT INTO test (script_name,sort_index,result) VALUES 
					('$script_name[$a]','$b','$indetific[$c]')
				");

				}

			}

		}

		public function Get()
		{
			include("assets/blocks/bd.php");
			
			$intoTable = $pdo->query('SELECT * FROM test WHERE result = "normal" OR result = "success"');

			

			return $intoTable;

			

		}

		

	}

	$myObj = new BaseClass();

	$into = $myObj->Get();

	
	$row = $into->fetch();
	echo "
<div class='container'>
        <div class='row'>
            <div class='col-sm-8 col-sm-offset-2'>
            <div class='page-header'>
                    <div class='alert alert-info' role='alert'>
                        <span>SELECT * FROM test WHERE result = 'normal' OR result = 'success'</span>
                    </div>
                </div>
<table border='2px'>
<thead>

			<tr>
			<th>#</th>
			<th>script_name</th>
			<th>start_time</th>
			<th>sort_index</th>
			<th>result</th>
			</tr>
			</thead>
			<tbody>

			";
			do
			{
			    
			    printf("<tr>
			    <td>%s</td>
			    <td>%s</td>
			    <td>%s</td>
			    <td>%s</td>
			    <td>%s</td>
			    </tr>  
			    ",$row['id'],$row['script_name'],$row['start_time'],$row['sort_index'],$row['result']);
			    
			    
			}
			while ($row = $into->fetch());


			echo "</tbody></table>
            </div>
        </div>
    </div>";

		
?>


