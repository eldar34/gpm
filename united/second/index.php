<?php 

echo '<a class="btn btn-primary" href="../../">Back</a>';

/*
* @directory - название дирректории в которой будет проходить поиск файлов
* @files - массив содержащий название файлов
*/

$directory = "datafiles";

$files = scandir($directory, 1);

/*
* getExtension2() - возвращает расширение файла 
*/

function getExtension2($filename) {
                  $path_info = pathinfo($filename, PATHINFO_EXTENSION);
                  return $path_info;
                  }

/*
* getFileName() - возвращает имя файла 
*/

function getFileName($filename) {
                  $path_info = pathinfo($filename, PATHINFO_FILENAME);
                  return $path_info;
                  }

/*
* @reg - регулярное выражение
* unitedResult() - возвращает отсартированный по регулярному выражению массив
*/

function unitedResult(array $props){

	$showExt = [];

	foreach ($props as $value) {

		$filename = getFileName($value);
		$fileExtension = getExtension2($value);

		$divide = explode('.', $filename);

		$united = $divide[0] . "." . $fileExtension;
		

		$reg = "/^([a-zA-Z-_0-9]*)\.(txt)$/i";

		$status = preg_match($reg, $united);

		if($status === 1){

			array_push($showExt, $united);

		}

		



		
	}

	return $showExt;

}


/*
* @row - массив файлов
*/

$row = unitedResult($files);
$i=1;

echo "<div class='container'>
        <div class='row'>
            <div class='col-sm-8 col-sm-offset-2'>
            <div class='page-header'>
                    <div class='alert alert-info' role='alert'>
                        <span>Filter with regular expression</span>
                    </div>
                </div>
<table border='2px'>
<thead>
			<tr>
			<th>#</th>
			<th>fileName</th>
			</tr>
			</thead>
			<tbody>

			";

			foreach ($row as $value) {
				echo "<tr><td>". $i ."</td><td>". $value ."</td></tr>";
				$i++;
			}
			

echo "</tbody></table>
            </div>
        </div>
    </div>";


?>

